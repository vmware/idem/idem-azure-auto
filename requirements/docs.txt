# python 3.8 is deprecated in sphinx 7.2.0 version
sphinx==7.1.2
sphinx-design
furo>=2022.6.21
sphinx-copybutton>=0.5.0
Sphinx-Substitution-Extensions>=2022.2.16
sphinx-notfound-page>=0.8.3
