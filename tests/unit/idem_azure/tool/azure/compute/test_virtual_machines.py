import copy


def _get_full_present_state():
    location = "location_value"
    virtual_machine_size = "virtual_machine_size_value"
    network_interface_ids = ["id1", "id2"]
    storage_image_reference = {
        "image_sku": "image_sku_value",
        "image_publisher": "image_publisher_value",
        "image_version": "image_version_value",
        "image_offer": "image_offer_value",
        "image_id": "image_id_value",
        "shared_gallery_image_id": "shared_gallery_image_id_value",
        "community_gallery_image_id": "community_gallery_image_id_value",
    }
    storage_os_disk = {
        "disk_name": "disk_name_value",
        "disk_caching": "disk_caching_value",
        "disk_create_option": "disk_create_option_value",
        "storage_account_type": "storage_account_type_value",
        "disk_delete_option": "disk_delete_option_value",
        "disk_size_in_GB": 30,
        "disk_id": "disk_id_value",
        "disk_image_vhd_uri": "disk_image_vhd_uri_value",
        "disk_os_type": "disk_os_type_value",
        "disk_vhd_uri": "disk_vhd_uri_value",
        "write_accelerator_enabled": False,
    }
    storage_data_disks = [
        {
            "disk_create_option": "disk_create_option_value",
            "disk_size_in_GB": 20,
            "disk_logical_unit_number": 7,
            "disk_name": "disk_name_value",
            "disk_caching": "disk_caching_value",
            "disk_delete_option": "disk_delete_option_value",
            "disk_id": "disk_id_value",
            "storage_account_type": "storage_account_type_value",
        },
        {
            "disk_create_option": "disk_create_option_value_2",
            "disk_size_in_GB": 21,
            "disk_logical_unit_number": 73,
            "disk_name": "disk_name_value_2",
            "disk_caching": "disk_caching_value_2",
            "disk_delete_option": "disk_delete_option_value_2",
            "disk_id": "disk_id_value_2",
            "storage_account_type": "storage_account_type_value_2",
        },
    ]
    os_profile = {
        "computer_name": "computer_name_value",
        "admin_username": "admin_username_value",
        "admin_password": "admin_password_value",
        "custom_data": "custom_data_value",
        "linux_configuration": {
            "ssh_public_keys": [
                {
                    "key_data": "key_data_1",
                    "path": "path_1",
                },
                {
                    "key_data": "key_data_2",
                    "path": "path_2",
                },
            ],
            "provision_vm_agent": True,
            "enable_vm_agent_platform_updates": False,
            "disable_password_authentication": True,
        },
        "windows_configuration": {
            "provision_vm_agent": True,
            "enable_vm_agent_platform_updates": False,
            "enable_automatic_updates": True,
            "time_zone": "time_zone_value",
        },
    }
    tags = {
        "tag_key_1": "tag_value_1",
        "tag_key_2": "tag_value_2",
    }
    plan = {
        "name": "name_value",
        "publisher": "publisher_value",
        "product": "product_value",
        "promotion_code": "promotion_code_value",
    }
    availability_set_id = "availability_set_id_value"
    license_type = "license_type_value"
    boot_diagnostics = {
        "enabled": False,
        "storage_uri": "storage_uri_value",
    }
    extensions_time_budget = "extensions_time_budget_value"

    state = {
        "location": location,
        "virtual_machine_size": virtual_machine_size,
        "network_interface_ids": network_interface_ids,
        "storage_image_reference": storage_image_reference,
        "storage_os_disk": storage_os_disk,
        "storage_data_disks": storage_data_disks,
        "os_profile": os_profile,
        "tags": tags,
        "plan": plan,
        "availability_set_id": availability_set_id,
        "license_type": license_type,
        "boot_diagnostics": boot_diagnostics,
        "extensions_time_budget": extensions_time_budget,
    }

    return state


def _get_raw_state():
    return {
        "plan": {
            "name": "raw_name",
            "product": "raw_product",
            "publisher": "raw_publisher",
            "promotionCode": "raw_promotionCode",
        },
        "location": "raw_location",
        "properties": {
            "licenseType": "raw_licenseType",
            "availabilitySet": {"id": "raw_id"},
            "diagnosticsProfile": {
                "bootDiagnostics": {
                    "enabled": True,
                    "storageUri": "raw_storageUri",
                }
            },
            "hardwareProfile": {
                "vmSize": "raw_vmSize",
            },
            "storageProfile": {
                "osDisk": {
                    "createOption": "raw_createOption",
                    "deleteOption": "raw_deleteOption",
                    "caching": "raw_caching",
                    "diskSizeGB": 101,
                    "managedDisk": {
                        "storageAccountType": "raw_storageAccountType",
                        "id": "raw_disk_id",
                    },
                    "writeAcceleratorEnabled": False,
                },
                "imageReference": {
                    "communityGalleryImageId": "raw_communityGalleryImageId",
                    "offer": "raw_offer",
                },
                "dataDisks": [
                    {
                        "createOption": "raw_createOption",
                        "deleteOption": "raw_deleteOption",
                        "managedDisk": {
                            "storageAccountType": "raw_storageAccountType_1",
                            "id": "raw_disk_id_1",
                        },
                        "diskSizeGB": 100,
                    },
                    {
                        "createOption": "raw_createOption",
                    },
                ],
            },
            "osProfile": {
                "adminUsername": "raw_adminUsername",
                "computerName": "raw_computerName",
                "adminPassword": "raw_adminPassword",
                "customData": "raw_customData",
                "linuxConfiguration": {
                    "enableVMAgentPlatformUpdates": True,
                    "provisionVMAgent": True,
                    "disablePasswordAuthentication": True,
                    "ssh": {
                        "publicKeys": [
                            {
                                "keyData": "raw_keyData_1",
                                "path": "raw_path_1",
                            },
                            {
                                "keyData": "raw_keyData_2",
                                "path": "raw_path_2",
                            },
                        ]
                    },
                },
                "windowsConfiguration": {
                    "enableAutomaticUpdates": True,
                    "enableVMAgentPlatformUpdates": False,
                    "provisionVMAgent": True,
                    "timeZone": "raw_timeZone",
                },
            },
            "extensionsTimeBudget": "raw_extensionsTimeBudget",
        },
    }


def test_convert_present_to_raw_virtual_machine_all_values_passed(hub):
    initial = _get_full_present_state()

    converted = (
        hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine(
            **initial
        )
    )

    asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        initial, converted
    )
    asserter("plan.name", "plan.name")
    asserter("plan.promotion_code", "plan.promotionCode")
    asserter(
        "boot_diagnostics.storage_uri",
        "properties.diagnosticsProfile.bootDiagnostics.storageUri",
    )
    asserter("license_type", "properties.licenseType")
    asserter("virtual_machine_size", "properties.hardwareProfile.vmSize")
    asserter("availability_set_id", "properties.availabilitySet.id")
    assert_os_profile(hub, converted, initial)
    asserter("extensions_time_budget", "properties.extensionsTimeBudget")


def test_update_virtual_machine_payload(hub):
    old_values = _get_full_present_state()
    existing_payload = (
        hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine(
            **old_values
        )
    )

    new_values = copy.deepcopy(old_values)
    new_values["plan"]["promotion_code"] = "new_promotion_code"
    new_values["virtual_machine_size"] = None
    new_values["license_type"] = None
    new_values["boot_diagnostics"]["enabled"] = True
    new_values["availability_set_id"] = "new_availability_set_id"
    new_values["os_profile"]["admin_password"] = "new_password"
    new_values["os_profile"]["linux_configuration"]["ssh_public_keys"].append(
        {
            "key_data": "new_key_data",
            "path": "new_key_path",
        }
    )
    new_values["extensions_time_budget"] = "new_extensions_time_budget"

    computed_update_payload = (
        hub.tool.azure.compute.virtual_machines.update_virtual_machine_payload(
            existing_payload=existing_payload,
            old_values_present=old_values,
            new_values_present=new_values,
        )
    )
    computed_update_payload = computed_update_payload.get("ret")

    new_value_asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        computed_update_payload, new_values
    )
    new_value_asserter("plan.name", "plan.name")
    new_value_asserter("plan.promotionCode", "plan.promotion_code")
    new_value_asserter("properties.availabilitySet.id", "availability_set_id")
    new_value_asserter(
        "properties.diagnosticsProfile.bootDiagnostics.enabled",
        "boot_diagnostics.enabled",
    )
    assert_os_profile(hub, computed_update_payload, new_values)
    new_value_asserter("properties.extensionsTimeBudget", "extensions_time_budget")

    old_value_asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        computed_update_payload, old_values
    )
    old_value_asserter("plan.name", "plan.name")
    old_value_asserter("properties.hardwareProfile.vmSize", "virtual_machine_size")
    old_value_asserter("properties.licenseType", "license_type")
    old_value_asserter(
        "properties.diagnosticsProfile.bootDiagnostics.storageUri",
        "boot_diagnostics.storage_uri",
    )


def test_convert_raw_virtual_machine_to_present(hub):
    raw_vm = _get_raw_state()

    idem_resource_name = "idem_resource_name_value"
    resource_group_name = "resource_group_name_value"
    virtual_machine_name = "virtual_machine_name_value"
    resource_id = "resource_id_value"
    subscription_id = "subscription_id_value"

    present_vm = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present(
            resource=raw_vm,
            idem_resource_name=idem_resource_name,
            resource_group_name=resource_group_name,
            virtual_machine_name=virtual_machine_name,
            resource_id=resource_id,
            subscription_id=subscription_id,
        )
    )

    assert present_vm["name"] == idem_resource_name
    assert present_vm["resource_id"] == resource_id
    assert present_vm["resource_group_name"] == resource_group_name
    assert present_vm["virtual_machine_name"] == virtual_machine_name
    assert present_vm["subscription_id"] == subscription_id

    asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        present_vm, raw_vm
    )
    asserter("location", "location")
    asserter("plan.name", "plan.name")
    asserter("plan.promotion_code", "plan.promotionCode")
    asserter("license_type", "properties.licenseType")
    asserter("availability_set_id", "properties.availabilitySet.id")
    asserter(
        "boot_diagnostics.enabled",
        "properties.diagnosticsProfile.bootDiagnostics.enabled",
    )
    asserter(
        "boot_diagnostics.storage_uri",
        "properties.diagnosticsProfile.bootDiagnostics.storageUri",
    )
    asserter(
        "storage_os_disk.disk_create_option",
        "properties.storageProfile.osDisk.createOption",
    )
    asserter(
        "storage_os_disk.disk_delete_option",
        "properties.storageProfile.osDisk.deleteOption",
    )
    asserter("storage_os_disk.disk_caching", "properties.storageProfile.osDisk.caching")
    asserter(
        "storage_os_disk.disk_size_in_GB", "properties.storageProfile.osDisk.diskSizeGB"
    )
    asserter(
        "storage_os_disk.write_accelerator_enabled",
        "properties.storageProfile.osDisk.writeAcceleratorEnabled",
    )
    asserter(
        "storage_os_disk.storage_account_type",
        "properties.storageProfile.osDisk.managedDisk.storageAccountType",
    )
    asserter(
        "storage_os_disk.disk_id", "properties.storageProfile.osDisk.managedDisk.id"
    )
    asserter(
        "storage_image_reference.community_gallery_image_id",
        "properties.storageProfile.imageReference.communityGalleryImageId",
    )
    asserter(
        "storage_image_reference.image_offer",
        "properties.storageProfile.imageReference.offer",
    )
    asserter("virtual_machine_size", "properties.hardwareProfile.vmSize")
    assert_os_profile(hub, raw_vm, present_vm)
    asserter("extensions_time_budget", "properties.extensionsTimeBudget")

    # manual check for dataDisks since current helpers don't support lists
    data_disks = present_vm.get("storage_data_disks")
    assert len(data_disks) == 2
    data_disk_0 = data_disks[0]
    data_disk_1 = data_disks[1]
    assert data_disk_0["disk_create_option"] == "raw_createOption"
    assert data_disk_0["disk_delete_option"] == "raw_deleteOption"
    assert data_disk_0["storage_account_type"] == "raw_storageAccountType_1"
    assert data_disk_0["disk_id"] == "raw_disk_id_1"
    assert data_disk_0["disk_size_in_GB"] == 100

    assert data_disk_1["disk_create_option"] == "raw_createOption"
    assert "disk_delete_option" not in data_disk_1
    assert "disk_id" not in data_disk_1


def assert_os_profile(hub, raw_state, present_state):
    asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        present_state, raw_state
    )
    asserter("os_profile.admin_username", "properties.osProfile.adminUsername")
    asserter("os_profile.admin_password", "properties.osProfile.adminPassword")
    asserter("os_profile.computer_name", "properties.osProfile.computerName")
    asserter("os_profile.custom_data", "properties.osProfile.customData")
    asserter(
        "os_profile.linux_configuration.ssh_public_keys[].key_data",
        "properties.osProfile.linuxConfiguration.ssh.publicKeys[].keyData",
    )
    asserter(
        "os_profile.linux_configuration.ssh_public_keys[].path",
        "properties.osProfile.linuxConfiguration.ssh.publicKeys[].path",
    )
    asserter(
        "os_profile.linux_configuration.provision_vm_agent",
        "properties.osProfile.linuxConfiguration.provisionVMAgent",
    )
    asserter(
        "os_profile.linux_configuration.enable_vm_agent_platform_updates",
        "properties.osProfile.linuxConfiguration.enableVMAgentPlatformUpdates",
    )
    asserter(
        "os_profile.linux_configuration.disable_password_authentication",
        "properties.osProfile.linuxConfiguration.disablePasswordAuthentication",
    )
    asserter(
        "os_profile.windows_configuration.provision_vm_agent",
        "properties.osProfile.windowsConfiguration.provisionVMAgent",
    )
    asserter(
        "os_profile.windows_configuration.enable_vm_agent_platform_updates",
        "properties.osProfile.windowsConfiguration.enableVMAgentPlatformUpdates",
    )
    asserter(
        "os_profile.windows_configuration.enable_automatic_updates",
        "properties.osProfile.windowsConfiguration.enableAutomaticUpdates",
    )
    asserter(
        "os_profile.windows_configuration.time_zone",
        "properties.osProfile.windowsConfiguration.timeZone",
    )
