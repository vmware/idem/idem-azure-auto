import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG


RESOURCE_NAME = "my-resource"
RESOURCE_TYPE = "network.virtual_networks"
RESOURCE_ID_TEMPLATE = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
RESOURCE_GROUP_NAME = "my-resource-group"
VIRTUAL_NETWORK_NAME = "my-vnet"
RESOURCE_ID_PROPERTIES = {
    "subscription_id": "my-sub-id",
    "virtual_network_name": VIRTUAL_NETWORK_NAME,
    "resource_group_name": RESOURCE_GROUP_NAME,
}
RESOURCE_PARAMETERS = {
    "address_space": ["10.12.13.0/25"],
    "location": "eastus",
    "extended_location": {"name": "abc", "type": "EdgeZone"},
    "flow_timeout_in_minutes": 10,
    "tags": {"tag-key": "tag-value"},
    "enable_vm_protection": True,
    "dhcp_options": {
        "dns_servers": ["sdfsdf.abc.com", "sdfsdf.xyz.com", "sdfsdf.lmn.com"]
    },
    "bgp_communities": {"virtual_network_community": "12076:20000"},
    "subnets": [
        {
            "name": "test vn-subnetName",
            "address_prefix": "10.12.13.0/16",
            "security_group_id": "security-group-id",
            "service_endpoints": ["Microsoft.Storage"],
        }
    ],
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "addressSpace": {"addressPrefixes": ["10.12.13.0/25"]},
        "flowTimeoutInMinutes": 10,
        "enableVmProtection": True,
        "dhcpOptions": {
            "dnsServers": ["sdfsdf.abc.com", "sdfsdf.xyz.com", "sdfsdf.lmn.com"]
        },
        "bgpCommunities": {"virtualNetworkCommunity": "12076:20000"},
        "subnets": [
            {
                "name": "test vn-subnetName",
                "properties": {
                    "addressPrefix": "10.12.13.0/16",
                    "networkSecurityGroup": {"id": "security-group-id"},
                    "serviceEndpoints": [{"service": "Microsoft.Storage"}],
                },
            }
        ],
    },
    "location": "eastus",
    "extendedLocation": {"name": "abc", "type": "EdgeZone"},
    "tags": {"tag-key": "tag-value"},
}

RESOURCE_PARAMETERS_UPDATE = {
    "address_space": ["10.12.13.0/25"],
    "location": "eastus",
    "extended_location": {"name": "abc", "type": "EdgeZone"},
    "flow_timeout_in_minutes": 15,
    "tags": {"tag-new-key": "tag-new-value"},
    "enable_vm_protection": False,
    "enable_ddos_protection": True,
    "ddos_protection_plan": {
        "id": "subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/ddosProtectionPlans/ddosProtectionPlanName"
    },
    "dhcp_options": {
        "dns_servers": [
            "sdfsdf.abc.com",
            "sdfsdf.xyz.com",
            "sdfsdf.123.com",
            "hello.org",
            "hi.bye",
        ]
    },
    "bgp_communities": {"virtual_network_community": "12076:20001"},
    "subnets": [
        {
            "name": "test-vn-subnetName-updated",
            "address_prefix": "10.12.13.0/16",
            "security_group_id": "security-group-id-updated",
            "service_endpoints": ["Microsoft.Storage"],
        }
    ],
}

RESOURCE_PARAMETERS_UPDATE_RAW = {
    "properties": {
        "addressSpace": {"addressPrefixes": ["10.12.13.0/25"]},
        "flowTimeoutInMinutes": 15,
        "enableVmProtection": False,
        "enableDdosProtection": True,
        "ddosProtectionPlan": {
            "id": "subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/ddosProtectionPlans/ddosProtectionPlanName"
        },
        "dhcpOptions": {
            "dnsServers": [
                "sdfsdf.abc.com",
                "sdfsdf.xyz.com",
                "sdfsdf.123.com",
                "hello.org",
                "hi.bye",
            ]
        },
        "bgpCommunities": {"virtualNetworkCommunity": "12076:20001"},
        "subnets": [
            {
                "name": "test-vn-subnetName-updated",
                "properties": {
                    "addressPrefix": "10.12.13.0/16",
                    "networkSecurityGroup": {"id": "security-group-id-updated"},
                    "serviceEndpoints": [{"service": "Microsoft.Storage"}],
                },
            }
        ],
    },
    "extendedLocation": {"name": "abc", "type": "EdgeZone"},
    "location": "eastus",
    "tags": {"tag-new-key": "tag-new-value"},
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, ctx):
    """
    Test 'present' state of virtual network. When a resource does not exist, 'present' should create the resource.
    """

    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, ctx):
    """
    Test 'present' state of virtual network. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """

    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE_RAW,
        RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of virtual networks. When a resource does not exist, 'absent' should just return success.
    """

    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of virtual networks. When a resource exists, 'absent' should delete the resource.
    """

    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    """
    Test 'describe' state of virtual networks.
    """

    list_url_format = "{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Network/virtualNetworks?api-version={api_version}"
    await unit_test_utils.test_describe_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        list_url_format=list_url_format,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
# same behaviour with or without resource id flag
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_resource_id_not_passed_successful_create(
    hub, ctx, fake_acct_data, __test, __resource_id_flag
):
    unit_test_utils.test_resource_id_not_passed_successful_create(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub, ctx, fake_acct_data, __test
):
    unit_test_utils.test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
        hub,
        ctx,
        fake_acct_data,
        __test,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )
