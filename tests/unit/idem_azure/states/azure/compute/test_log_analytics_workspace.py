import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
WORKSPACE_NAME = "my-workspace"
SUBSCRIPTION_ID = "12345678-1234-1234-1234-aaabc1234aaa"
LOCATION = "eastus"
RESOURCE_PARAMETERS = {
    "public_network_access_for_ingestion": "Enabled",
    "public_network_access_for_query": "Enabled",
    "sku": {"last_sku_update": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
    "features": {"enable_log_access_using_only_resource_permissions": True},
    "workspace_capping": {
        "daily_quota_gb": -1.0,
        "data_ingestion_status": "RespectQuota",
        "quota_next_reset_time": "2022-12-13T08:00:00Z",
    },
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
}

RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    "properties": {
        "retentionInDays": 30,
        "publicNetworkAccessForIngestion": "Enabled",
        "publicNetworkAccessForQuery": "Enabled",
        "features": {"enableLogAccessUsingOnlyResourcePermissions": True},
        "sku": {"lastSkuUpdate": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
        "workspaceCapping": {
            "dailyQuotaGb": -1.0,
            "dataIngestionStatus": "RespectQuota",
            "quotaNextResetTime": "2022-12-13T08:00:00Z",
        },
    },
    "type": "Microsoft.OperationalInsights/workspaces",
}

RESOURCE_PARAMETERS_UPDATE = {
    "public_network_access_for_ingestion": "Enabled",
    "public_network_access_for_query": "Enabled",
    "sku": {"last_sku_update": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
    "features": {"enable_log_access_using_only_resource_permissions": True},
    "workspace_capping": {
        "daily_quota_gb": -1.0,
        "data_ingestion_status": "RespectQuota",
        "quota_next_reset_time": "2022-12-13T08:00:00Z",
    },
    "tags": {"tag-new-key": "tag-new-value"},
}

RESOURCE_PARAMETERS_UPDATE_RAW = {
    "location": "eastus",
    "tags": {"tag-new-key": "tag-new-value"},
    "properties": {
        "retentionInDays": 30,
        "publicNetworkAccessForIngestion": "Enabled",
        "publicNetworkAccessForQuery": "Enabled",
        "features": {"enableLogAccessUsingOnlyResourcePermissions": True},
        "sku": {"lastSkuUpdate": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
        "workspaceCapping": {
            "dailyQuotaGb": -1.0,
            "dataIngestionStatus": "RespectQuota",
            "quotaNextResetTime": "2022-12-13T08:00:00Z",
        },
    },
    "type": "Microsoft.OperationalInsights/workspaces",
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of log analytics. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.compute.log_analytics_workspace.present = (
        hub.states.azure.compute.log_analytics_workspace.present
    )
    mock_hub.exec.azure.compute.log_analytics_workspace.get = (
        hub.exec.azure.compute.log_analytics_workspace.get
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present = (
        hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_present_log_analytics_workspace_to_raw = (
        hub.tool.azure.compute.log_analytics_workspace.convert_present_log_analytics_workspace_to_raw
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.OperationalInsights/workspaces/{WORKSPACE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.compute.log_analytics_workspace",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert WORKSPACE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert WORKSPACE_NAME in url
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.compute.log_analytics_workspace.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SUBSCRIPTION_ID,
        WORKSPACE_NAME,
        LOCATION,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.compute.log_analytics_workspace '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    # Test present() with --test flag off
    test_ctx["test"] = False
    ret = await mock_hub.states.azure.compute.log_analytics_workspace.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SUBSCRIPTION_ID,
        WORKSPACE_NAME,
        LOCATION,
        **RESOURCE_PARAMETERS,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.compute.log_analytics_workspace '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of log analytics workspace. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.compute.log_analytics_workspace.present = (
        hub.states.azure.compute.log_analytics_workspace.present
    )
    mock_hub.exec.azure.compute.log_analytics_workspace.get = (
        hub.exec.azure.compute.log_analytics_workspace.get
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present = (
        hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_present_log_analytics_workspace_to_raw = (
        hub.tool.azure.compute.log_analytics_workspace.convert_present_log_analytics_workspace_to_raw
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.update_log_analytics_workspace_payload = (
        hub.tool.azure.compute.log_analytics_workspace.update_log_analytics_workspace_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.OperationalInsights/workspaces/{WORKSPACE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.OperationalInsights/workspaces/{WORKSPACE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_UPDATE_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert WORKSPACE_NAME in url
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "retentionInDays"
        ] == json.get("properties").get("retentionInDays")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "publicNetworkAccessForIngestion"
        ] == json.get("properties").get("publicNetworkAccessForIngestion")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "publicNetworkAccessForQuery"
        ] == json.get("properties").get("publicNetworkAccessForQuery")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["tags"] == json.get("tags")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["location"] == json.get("location")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.compute.log_analytics_workspace.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        SUBSCRIPTION_ID,
        WORKSPACE_NAME,
        LOCATION,
        **RESOURCE_PARAMETERS_UPDATE,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of log analytics. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.compute.log_analytics_workspace.absent = (
        hub.states.azure.compute.log_analytics_workspace.absent
    )
    mock_hub.exec.azure.compute.log_analytics_workspace.get = (
        hub.exec.azure.compute.log_analytics_workspace.get
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present = (
        hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert WORKSPACE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.compute.log_analytics_workspace.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, WORKSPACE_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.compute.log_analytics_workspace '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of log analytics workspace. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.compute.log_analytics_workspace.absent = (
        hub.states.azure.compute.log_analytics_workspace.absent
    )
    mock_hub.exec.azure.compute.log_analytics_workspace.get = (
        hub.exec.azure.compute.log_analytics_workspace.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present = (
        hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.OperationalInsights/workspaces/{WORKSPACE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_UPDATE_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert WORKSPACE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.compute.log_analytics_workspace.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, WORKSPACE_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.compute.log_analytics_workspace '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS_UPDATE,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.compute.log_analytics_workspace.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, WORKSPACE_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.compute.log_analytics_workspace '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS_UPDATE,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of log analytics.
    """
    mock_hub.states.azure.compute.log_analytics_workspace.describe = (
        hub.states.azure.compute.log_analytics_workspace.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present = (
        hub.tool.azure.compute.log_analytics_workspace.convert_raw_log_analytics_workspace_to_present
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.OperationalInsights/workspaces/{WORKSPACE_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_NAME,
                    **RESOURCE_PARAMETERS_UPDATE_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.compute.log_analytics_workspace.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.compute.log_analytics_workspace.present" in ret_value.keys()

    described_resource = ret_value.get("azure.compute.log_analytics_workspace.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS_UPDATE,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert WORKSPACE_NAME == old_state.get("workspace_name")
        assert expected_old_state[
            "public_network_access_for_ingestion"
        ] == old_state.get("public_network_access_for_ingestion")
        assert expected_old_state["public_network_access_for_query"] == old_state.get(
            "public_network_access_for_query"
        )
        assert expected_old_state["features"] == old_state.get("features")
        assert expected_old_state["workspace_capping"] == old_state.get(
            "workspace_capping"
        )
        assert expected_old_state["sku"] == old_state.get("sku")
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert WORKSPACE_NAME == new_state.get("workspace_name")
        assert expected_new_state[
            "public_network_access_for_ingestion"
        ] == new_state.get("public_network_access_for_ingestion")
        assert expected_new_state["public_network_access_for_query"] == new_state.get(
            "public_network_access_for_query"
        )
        assert expected_new_state["features"] == new_state.get("features")
        assert expected_new_state["workspace_capping"] == new_state.get(
            "workspace_capping"
        )
        assert expected_new_state["sku"] == new_state.get("sku")
        assert expected_new_state["tags"] == new_state.get("tags")
