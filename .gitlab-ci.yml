---
stages:
  - pre-commit
  - test-suite
  - test-suite-state-integration
  - pkg
  - pop-release

include:
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/url-tester-brok.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /lint/pre-commit-run-all.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /release/pop_release.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/build-docs-html-nox.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-gitlab.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-release.yml

variables:
  CICD_UPSTREAM_PATH: "vmware/idem/idem-azure"

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID == null

pre-commit-run-all:
  image: registry.gitlab.com/saltstack/pop/cicd/containers/alpinelint:py39

.test-suite:
  stage: test-suite
  needs:
    - pre-commit-run-all
  script:
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/unit tests/integration/autogen
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days

.test-suite-state-integration:
  stage: test-suite-state-integration
  needs:
    - pre-commit-run-all
    - tests-3.8
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days
  image: python:3.8
  when: manual

tests-3.8:
  extends: .test-suite
  image: python:3.8

tests-3.9:
  extends: .test-suite
  image: python:3.9

tests-3.10:
  extends: .test-suite
  image: python:3.10.2

tests-3.11:
  extends: .test-suite
  image: python:3.11

# Run this on real azure after merge requests
# In gitlab go to your fork of idem-azure -> Settings -> CI / CD -> Variables
# Create a File key called "ACCT_FILE" that contains the contents of your encrypted fernet file
# Create a Variable key called "ACCT_KEY" that contains the key for decrypting your acct file
tests-authorization:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/authorization/

tests-state-compute:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/compute/

tests-exec-compute:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/exec/compute/

tests-management_groups:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/management_groups/

tests-policy:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/policy/

tests-resource_management:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/resource_management/

tests-subscription:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/subscription/

tests-key_vault:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/key_vault/

tests-network:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/network/

tests-storage_resource_provider:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/storage_resource_provider/

tests-sql_database:
  extends: .test-suite-state-integration
  script:
    - python3 -m pip install nox
    - nox -e tests-3.8 -- -vv tests/integration/states/sql_database/

build-docs-html:
  stage: test-suite

pages:
  variables:
    CICD_DOCS_VERSION_LATEST: latest
  stage: pkg

publish-docs:
  variables:
    CICD_S3_DEST_PATH: "docs.idemproject.io/idem-azure/"
