State
=====

The *State* command allows you to ensure your environment is configured how you
specify. You specify your configuration in an *SLS* file, also known as a
*state* file.

Describe Example
++++++++++++++++

First let's describe our Resource Groups.

.. code-block:: bash

    idem describe azure.resource_management.resource_groups

Output:

.. code-block:: bash

    /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1:
      azure.resource_management.resource_groups.present:
      - name: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_id: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_group_name: test-rg1
      - subscription_id: b8e40357-9bb6-4c41-9b43-a9ba0fd08160
      - location: eastus
      - tags:
          name: test-idem

Use Idem State
______________

You'll notice that `tags` is having a single key-value pair `name: test-idem`. If I want to
change it to `name: updated-value` and add a new tag `env: test-env` I can take the above output from the Describe
example and create a file with `.sls` extension and use that to update the
running configuration.

Copy the above text output to ~/my_rg.sls, change `tags` to:

.. code-block:: bash

    /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1:
      azure.resource_management.resource_groups.present:
      - name: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_id: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_group_name: test-rg1
      - subscription_id: b8e40357-9bb6-4c41-9b43-a9ba0fd08160
      - location: eastus
      - tags:
          name: updated-value
          env: test-env

and then run the following command:

.. code-block:: bash

    idem state ~/my_rg.sls

Output:

.. code-block:: yaml

    --------
          ID: subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
    Function: azure.resource_management.resource_groups.present
      Result: True
     Comment: ("Updated azure.resource_management.resource_groups '/subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1'", "azure.resource_management.resource_groups '/subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1' has no property need to be updated.")
     Changes:
    old:
        ----------
        tags:
            ----------
            name:
                test-idem
    new:
        ----------
        tags:
            ----------
            name:
                updated-value
            env:
                test-env


    present: 1 updated successfully


Here you'll notice that the `old` field shows us the previous value for
`tags` and the `new` field shows us the new value.
